# Privacy Policy

F-Droid Nearby does not collect or transmit any personal information. In the case of an application crash, F-Droid Nearby may show a dialog window asking you to report the issue, in which case potentionally identifying device information may be sent. This, however, will only happen if you explicitly choose to send us crash information.

# Changes

This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes.

# Contact us

If you have any questions regarding privacy while using the application, or have questions about our practices, please contact us via email at team@f-droid.org.
