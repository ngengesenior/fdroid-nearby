package org.fdroid.fdroid.installer;

import android.content.Context;

import java.io.File;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class ApkCache {
    public static File getApkDownloadPath(Context context, String canonicalUrl) {
        throw new IllegalArgumentException("unimplemented");
    }
}
