package org.fdroid.fdroid.data;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class CategoryProvider {
    public static class Helper {
        public static void clearCategoryIdCache() {
        }
    }
}
