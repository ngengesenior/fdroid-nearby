package org.fdroid.fdroid.nearby;

import android.content.Context;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class BluetoothManager {

    public static final String ACTION_FOUND = "BluetoothNewPeer";
    public static final String EXTRA_PEER = "extraBluetoothPeer";

    public static final String ACTION_STATUS = "BluetoothStatus";
    public static final String EXTRA_STATUS = "BluetoothStatusExtra";
    public static final int STATUS_STARTING = 0;
    public static final int STATUS_STARTED = 1;
    public static final int STATUS_STOPPING = 2;
    public static final int STATUS_STOPPED = 3;
    public static final int STATUS_ERROR = 0xffff;

    public static void stop(Context context) {
        // no op
    }

    public static void start(final Context context) {
        // no op
    }

    public static boolean isAlive() {
        return false;
    }
}
