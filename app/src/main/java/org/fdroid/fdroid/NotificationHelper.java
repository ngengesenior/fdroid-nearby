package org.fdroid.fdroid;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import java.util.Collections;

import androidx.core.app.NotificationManagerCompat;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class NotificationHelper {
    public static final String CHANNEL_SWAPS = "swap-channel";

    private final Context context;
    private final NotificationManagerCompat notificationManager;

    NotificationHelper(Context context) {
        this.context = context;
        notificationManager = NotificationManagerCompat.from(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationChannel swapChannel = new NotificationChannel(CHANNEL_SWAPS,
                    context.getString(R.string.notification_channel_swaps_title),
                    NotificationManager.IMPORTANCE_LOW);
            swapChannel.setDescription(context.getString(R.string.notification_channel_swaps_description));

            notificationManager.createNotificationChannels(Collections.singletonList(swapChannel));
        }
    }
}
