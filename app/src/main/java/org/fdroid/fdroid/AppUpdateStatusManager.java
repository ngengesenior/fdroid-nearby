package org.fdroid.fdroid;

import android.app.PendingIntent;
import android.content.Context;

import java.util.Collection;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public final class AppUpdateStatusManager {

    public enum Status {
        PendingInstall,
        DownloadInterrupted,
        UpdateAvailable,
        Downloading,
        ReadyToInstall,
        Installing,
        Installed,
        InstallError,
    }

    public static class AppUpdateStatus {
        public String getCanonicalUrl() {
            throw new IllegalStateException("unimplemented");
        }
    }

    public static AppUpdateStatusManager getInstance(Context context) {
        if (instance == null) {
            instance = new AppUpdateStatusManager();
        }
        return instance;
    }

    private static AppUpdateStatusManager instance;

    public void updateApk(String canonicalUrl, @NonNull Status status, @Nullable PendingIntent pendingIntent) {
        // no op
    }

    public Collection<AppUpdateStatus> getByPackageName(String packageName) {
        return Collections.EMPTY_LIST; // no op
    }
}
