package org.fdroid.fdroid;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class IndexUpdater {
    public static final String SIGNED_FILE_NAME = "index.jar";
    public static final String DATA_FILE_NAME = "index.xml";
}
