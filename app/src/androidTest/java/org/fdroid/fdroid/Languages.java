package org.fdroid.fdroid;

import android.content.ContextWrapper;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public final class Languages {
    public static final String TAG = "Languages";

    private static final Locale TIBETAN = new Locale("bo");
    private static final Locale CHINESE_HONG_KONG = new Locale("zh", "HK");

    static {
        // no op
    }

    private Languages(AppCompatActivity activity) {
        // no op
    }

    public static Languages get(AppCompatActivity activity) {
        return null; // no op
    }

    public static void setLanguage(final ContextWrapper contextWrapper) {
        // no op
    }

    public static void forceChangeLanguage(AppCompatActivity activity) {
        // no op
    }

    public String[] getAllNames() {
        return null; // no op
    }

    public String[] getSupportedLocales() {
        return null; // no op
    }

    private String capitalize(final String line) {
        return null; // no op
    }

    public static final Locale[] LOCALES_TO_TEST = {
            Locale.ENGLISH,
            Locale.FRENCH,
            Locale.GERMAN,
            Locale.ITALIAN,
            Locale.JAPANESE,
            Locale.KOREAN,
            Locale.SIMPLIFIED_CHINESE,
            Locale.TRADITIONAL_CHINESE,
            CHINESE_HONG_KONG,
            TIBETAN,
            new Locale("af"),
            new Locale("ar"),
            new Locale("ast"),
            new Locale("be"),
            new Locale("bg"),
            new Locale("bn"),
            new Locale("ca"),
            new Locale("cs"),
            new Locale("cy"),
            new Locale("da"),
            new Locale("el"),
            new Locale("eo"),
            new Locale("es"),
            new Locale("et"),
            new Locale("eu"),
            new Locale("fa"),
            new Locale("fi"),
            new Locale("gl"),
            new Locale("he"),
            new Locale("hi"),
            new Locale("hr"),
            new Locale("hu"),
            new Locale("hy"),
            new Locale("id"),
            new Locale("is"),
            new Locale("it"),
            new Locale("ja"),
            new Locale("kab"),
            new Locale("kn"),
            new Locale("ko"),
            new Locale("lt"),
            new Locale("lv"),
            new Locale("ml"),
            new Locale("mn"),
            new Locale("mr"),
            new Locale("my"),
            new Locale("nb"),
            new Locale("nl"),
            new Locale("nn"),
            new Locale("pl"),
            new Locale("pt"),
            new Locale("ro"),
            new Locale("ru"),
            new Locale("sc"),
            new Locale("sk"),
            new Locale("sl"),
            new Locale("sn"),
            new Locale("sq"),
            new Locale("sr"),
            new Locale("sv"),
            new Locale("th"),
            new Locale("ta"),
            new Locale("te"),
            new Locale("tr"),
            new Locale("uk"),
            new Locale("vi"),
    };

}
