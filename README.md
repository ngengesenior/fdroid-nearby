
F-Droid Nearby is a simple app for exchanging free software apps locally,
device-to-device, even when internet is not available or too expensive.  It is
compatible with the built-in Nearby feature of the F-Droid client app.
